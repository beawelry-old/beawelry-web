class Mailer < ActionMailer::Base
  default from: 'Beawelry Website <engrave@beawelry.com>'

  def enquiry_mail(data)
    @data = data
    mail(to: 'customer@beawelry.com', subject: '[ENQUIRY] A message from beawelry.com')
  end
end
