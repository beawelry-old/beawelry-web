class NewsController < ApplicationController
  before_action :change_page_group

  def index
    set_page_title "News | Beawelry"
  end

  def grand_opening
    set_page_title "Grand Opening | Beawelry"
  end

  private

  def change_page_group
    set_page_group 'news'
  end
end
