class InsidesController < ApplicationController
  def show
    set_page_title "Inside | Beawelry"
    set_page_group "inside"
  end
end
