class TermsController < ApplicationController
  before_action :change_page_group

  def terms_and_policies
    set_page_title "Terms & Policies | Beawelry"
  end

  private

  def change_page_group
    set_page_group "terms"
  end
end
