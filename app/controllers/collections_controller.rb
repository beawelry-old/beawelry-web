class CollectionsController < ApplicationController
  before_action :change_page_group

  def index
    set_page_title "Collections | Beawelry"
  end

  private

  def change_page_group
    set_page_group "collections"
  end
end
