class WelcomeController < ApplicationController
  layout 'welcome'

  def landing
    render template: 'welcome/landing', layout: false
  end

  def coming_soon
    set_page_title "Coming Soon"
    render template: 'welcome/coming_soon', layout: false
  end

  def new_coming_soon
    set_page_title "Bare your heart | Beawelry"
    render template: 'welcome/new_coming_soon', layout: false
  end
end
