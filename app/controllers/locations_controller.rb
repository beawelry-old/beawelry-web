class LocationsController < ApplicationController
  before_action :change_page_group

  def show
    set_page_title "Store Location | Beawelry"
  end

  def leave_message
    name = params[:name] || ''
    email = params[:email] || ''
    message   = params[:message] || []

    if name.blank? or email.blank? or message.strip.blank?
      render text: ''
      return
    end

    telephone = params[:telephone] || ''

    Mailer.enquiry_mail({ name: name, email: email, message: message, telephone: telephone }).deliver
  end

  private

  def change_page_group
    set_page_group 'location'
  end
end
