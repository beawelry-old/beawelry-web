class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :set_page_title
  helper_method :set_page_group
  helper_method :page_title

  protected

  def set_page_title(title = '')
    @page_title = title
  end

  def set_page_group(group)
    @page_group = group
  end

  def page_title
    @page_title.blank? ? 'Beawelry' : @page_title
  end
end
