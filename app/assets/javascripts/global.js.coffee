Global = {
  startCarousel: ->
    $('.carousel').carousel({
      interval: false,
      pause: true,
      wrap: false
    })

    # Function: Hide/Show carousel controls depending on active item
    checkitem = ->
      $this = $(".carousel");
      if ($(".carousel .carousel-inner .item:first").hasClass("active"))
        $this.children(".left").hide();
        $this.children(".right").show();
      else if ($(".carousel .carousel-inner .item:last").hasClass("active"))
        $this.children(".right").hide();
        $this.children(".left").show();
      else
        $this.children(".carousel-control").show();

    # Check active slide at the run
    checkitem();

    # Check active item at every slide moves
    $(".carousel").on("slid.bs.carousel", "", checkitem);
  init: ->
    Global.startCarousel()
}

$(Global.init)
