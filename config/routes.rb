Rails.application.routes.draw do

  resource :inside, only: [:show]
  resources :collections, only: [:index]
  resources :news, only: [:index] do
    collection do
      get "grand-opening" => "news#grand_opening"
    end
  end

  resource :location, only: [:show] do
    collection do
      post "leave-message" => "locations#leave_message"
    end
  end

  get "/terms-and-policies" => "terms#terms_and_policies"

  root to: "welcome#new_coming_soon"
end
